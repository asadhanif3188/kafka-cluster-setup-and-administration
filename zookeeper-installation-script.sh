#!/bin/bash

# Packages
sudo apt-get update && \
      sudo apt-get -y install wget ca-certificates zip net-tools vim nano tar netcat

# Java Open JDK 8
sudo apt-get -y install openjdk-8-jdk
java -version

# Disable RAM Swap - can set to 0 on certain Linux distro
sudo sysctl vm.swappiness=1
echo 'vm.swappiness=1' | sudo tee --append /etc/sysctl.conf

# Add hosts entries (mocking DNS) - put relevant IPs here
echo "172.31.9.1 kafka1
172.31.9.1 zookeeper1
172.31.19.230 kafka2
172.31.19.230 zookeeper2
172.31.35.20 kafka3
172.31.35.20 zookeeper3" | sudo tee --append /etc/hosts

# download Zookeeper and Kafka. Recommended is latest Kafka (3.7.0) and Scala 2.12
wget https://archive.apache.org/dist/kafka/3.7.0/kafka_2.12-3.7.0.tgz
tar -xvzf kafka_2.12-3.7.0.tgz
rm kafka_2.12-3.7.0.tgz
mv kafka_2.12-3.7.0 kafka
ls kafka