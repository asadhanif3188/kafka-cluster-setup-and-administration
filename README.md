# Kafka Cluster Setup and Administration

**ZooKeeper** Quorum Setup, **Kafka Cluster** Setup and Administration in AWS


## Important Note

To properly setup a Kafka Cluster, we need;
1. Zookeeper cluster
2. Kafka cluster
3. Proper replication factor
4. Multi Availability Zones setup
5. Proper configurations 
6. Administration tools
    - ZooNavigator 
    - Kafka Manager
    - Kafka Topics UI
    - Confluent Schema Registry 
    - Confluent REST Proxy 

## Kafka Architecture 

<img src="./images/Kafka-Final-Architecture.png" width="90%" > 

## Zookeeper

- Zookeeper High Level
    - Distributed key value store
    - Has voting mechanisms 
    - Used by many big data tools

- Zookeeper version 3.4 is stable.
- Zookeeper internal structure is tree
- Each node is called zNode 
- Each node can store data

### Role of Zookeeper 
- Broker registration, with heartbeat mechanism to keep the list upto date.
- Maintaining a list of topics alongside and their configurations 
    - partitions 
    - replication factor  
    - list of ISR (in-sync replicas) for partitions 
- Performing leader elections in case some brokers go down 
- Storing the Kafka cluster ID 
- Storing ACLs (Access Control Lists) if security is enabled 
    - topics
    - consumer groups 
    - users
- Quotas configuration if enabled 
- 

## VPC and Subnets 
We are using Ireland (`eu-west-1`) region to deploy our clusters. 

**IP CIDR**     **AZ**
172.31.0.0/20   eu-west-1a  
172.31.16.0/20  eu-west-1b
172.31.32.0/20  eu-west-1c

### server 1 configs
AZ:         eu-west-1b
Private IP: 172.31.9.1

### server 2 configs
AZ:         eu-west-1c
Private IP: 172.31.19.230

### server 3 configs
AZ:         eu-west-1a
Private IP: 172.31.35.20

## Steps to Install Zookeeper 

#### Step 01 - Install Packages
```
# Packages
sudo apt-get update && \
      sudo apt-get -y install wget ca-certificates zip net-tools vim nano tar netcat
```

#### Step 02 - Install Java Open JDK 8
```
# Java Open JDK 8
sudo apt-get -y install openjdk-8-jdk
java -version
```

#### Step 03 - Disable RAM Swap
```
# Disable RAM Swap - can set to 0 on certain Linux distro
sudo sysctl vm.swappiness=1
echo 'vm.swappiness=1' | sudo tee --append /etc/sysctl.conf
```

#### Step 04 - Add hosts entries (mocking DNS)
```
# Add hosts entries (mocking DNS) - put relevant IPs here
echo "172.31.9.1 kafka1
172.31.9.1 zookeeper1
172.31.19.230 kafka2
172.31.19.230 zookeeper2
172.31.35.20 kafka3
172.31.35.20 zookeeper3" | sudo tee --append /etc/hosts
```

#### Step 05 - Download Zookeeper and Kafka
```
# download Zookeeper and Kafka. Recommended is latest Kafka (3.7.0) and Scala 2.12
wget https://archive.apache.org/dist/kafka/3.7.0/kafka_2.12-3.7.0.tgz
tar -xvzf kafka_2.12-3.7.0.tgz
rm kafka_2.12-3.7.0.tgz
mv kafka_2.12-3.7.0 kafka
cd kafka/

```

#### Step 06 - Zookeeper quickstart (optional)
This step is just for testing purpose. 
```
# Zookeeper quickstart
cat config/zookeeper.properties
bin/zookeeper-server-start.sh config/zookeeper.properties
```

#### Step 07 - Start Zookeeper in the background (optional)
```
# Testing Zookeeper install
# Start Zookeeper in the background
bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
bin/zookeeper-shell.sh localhost:2181
ls /
# demonstrate the use of a 4 letter word
echo "ruok" | nc localhost 2181 ; echo
```

#### Step 08 - Start Zookeeper as a Service 

**Script to start and stop the service**

Copy following script in `/etc/init.d/zookeeper` file.

```
#!/bin/sh
#
# zookeeper          Start/Stop zookeeper
#
# chkconfig: - 99 10
# description: Standard script to start and stop zookeeper

DAEMON_PATH=/home/ubuntu/kafka/bin
DAEMON_NAME=zookeeper

PATH=$PATH:$DAEMON_PATH

# See how we were called.
case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Zookeeper is already running";
        else
          echo "Starting $DAEMON_NAME";
          $DAEMON_PATH/zookeeper-server-start.sh -daemon /home/ubuntu/kafka/config/zookeeper.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME";
        $DAEMON_PATH/zookeeper-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Zookeeper is Running as PID: $pid"
        else
          echo "Zookeeper is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
```

**Change the ownership and make it executable**

```
sudo chmod +x /etc/init.d/zookeeper
sudo chown root:root /etc/init.d/zookeeper
```

**Update the services**

```
# you can safely ignore the warning
sudo update-rc.d zookeeper defaults
```

**Start the service**
```
sudo service zookeeper start
```

**Verify it's started**
```
nc -vz localhost 2181
echo "ruok" | nc localhost 2181 ; echo
```

**Note:** In case you encounter following error/warning. 

_**ruok is not executed because it is not in the whitelist.**_

Just add following lines to `config/zookeeper.properties`

```
4lw.commands.whitelist=stat, ruok, conf, isro
4lw.commands.whitelist=*
```

**Start zookeeper shell**

```
bin/zookeeper-shell.sh localhost:2181
ls /
```

**Stop the service**
```
sudo service zookeeper stop
```

**Check the logs**

```
# check the logs
cat logs/zookeeper.out
```


#### Zookeeper Installation Script


```
#!/bin/bash

# Packages
sudo apt-get update && \
      sudo apt-get -y install wget ca-certificates zip net-tools vim nano tar netcat

# Java Open JDK 8
sudo apt-get -y install openjdk-8-jdk
java -version

# Disable RAM Swap - can set to 0 on certain Linux distro
sudo sysctl vm.swappiness=1
echo 'vm.swappiness=1' | sudo tee --append /etc/sysctl.conf

# Add hosts entries (mocking DNS) - put relevant IPs here
echo "172.31.9.1 kafka1
172.31.9.1 zookeeper1
172.31.19.230 kafka2
172.31.19.230 zookeeper2
172.31.35.20 kafka3
172.31.35.20 zookeeper3" | sudo tee --append /etc/hosts

# download Zookeeper and Kafka. Recommended is latest Kafka (3.7.0) and Scala 2.12
wget https://archive.apache.org/dist/kafka/3.7.0/kafka_2.12-3.7.0.tgz
tar -xvzf kafka_2.12-3.7.0.tgz
rm kafka_2.12-3.7.0.tgz
mv kafka_2.12-3.7.0 kafka
cd kafka/

```

#### Zookeeper Service Code

```
#!/bin/sh
#
# zookeeper          Start/Stop zookeeper
#
# chkconfig: - 99 10
# description: Standard script to start and stop zookeeper

DAEMON_PATH=/home/ubuntu/kafka/bin
DAEMON_NAME=zookeeper

PATH=$PATH:$DAEMON_PATH

# See how we were called.
case "$1" in
  start)
        # Start daemon.
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
            echo "Zookeeper is already running";
        else
          echo "Starting $DAEMON_NAME";
          $DAEMON_PATH/zookeeper-server-start.sh -daemon /home/ubuntu/kafka/config/zookeeper.properties
        fi
        ;;
  stop)
        echo "Shutting down $DAEMON_NAME";
        $DAEMON_PATH/zookeeper-server-stop.sh
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'org.apache.zookeeper' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Zookeeper is Running as PID: $pid"
        else
          echo "Zookeeper is not Running"
        fi
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac

exit 0
```

#### Install Zookeeper boot scripts 
```
# Install Zookeeper boot scripts
sudo nano /etc/init.d/zookeeper
sudo chmod +x /etc/init.d/zookeeper
sudo chown root:root /etc/init.d/zookeeper
# you can safely ignore the warning
sudo update-rc.d zookeeper defaults
# stop zookeeper
sudo service zookeeper stop
# verify it's stopped
nc -vz localhost 2181
# start zookeeper
sudo service zookeeper start
# verify it's started
nc -vz localhost 2181
echo "ruok" | nc localhost 2181 ; echo
# check the logs
cat logs/zookeeper.out

```

<!-- #### Complete Script 
```
#!/bin/bash

# Packages
sudo apt-get update && \
      sudo apt-get -y install wget ca-certificates zip net-tools vim nano tar netcat

# Java Open JDK 8
sudo apt-get -y install openjdk-8-jdk
java -version

# Disable RAM Swap - can set to 0 on certain Linux distro
sudo sysctl vm.swappiness=1
echo 'vm.swappiness=1' | sudo tee --append /etc/sysctl.conf

# Add hosts entries (mocking DNS) - put relevant IPs here
echo "172.31.9.1 kafka1
172.31.9.1 zookeeper1
172.31.19.230 kafka2
172.31.19.230 zookeeper2
172.31.35.20 kafka3
172.31.35.20 zookeeper3" | sudo tee --append /etc/hosts

# Zookeeper quickstart
cat config/zookeeper.properties
bin/zookeeper-server-start.sh config/zookeeper.properties
# binding to port 2181 -> you're good. Ctrl+C to exit

# Testing Zookeeper install
# Start Zookeeper in the background
bin/zookeeper-server-start.sh -daemon config/zookeeper.properties
bin/zookeeper-shell.sh localhost:2181
ls /
# demonstrate the use of a 4 letter word
echo "ruok" | nc localhost 2181 ; echo

# Install Zookeeper boot scripts
sudo nano /etc/init.d/zookeeper
sudo chmod +x /etc/init.d/zookeeper
sudo chown root:root /etc/init.d/zookeeper
# you can safely ignore the warning
sudo update-rc.d zookeeper defaults
# stop zookeeper
sudo service zookeeper stop
# verify it's stopped
nc -vz localhost 2181
# start zookeeper
sudo service zookeeper start
# verify it's started
nc -vz localhost 2181
echo "ruok" | nc localhost 2181 ; echo
# check the logs
cat logs/zookeeper.out

``` -->

## Working with Zookeeper Shell

```
nc -vz localhost 2181

# connect to shell
bin/zookeeper-shell.sh localhost:2181

# display help
help
# display root
ls /
create /my-node "foo"
ls /
get /my-node
get /zookeeper
create /my-node/deeper-node "bar"
ls /
ls /my-node
ls /my-node/deeper-node
get /my-node/deeper-node
# update data version to see increased version counter
set /my-node/deeper-node "newdata"
get /my-node/deeper-node
# removes are recursive
deleteall /my-node
ls /
# create a watcher
create /node-to-watch ""
get /node-to-watch true
set /node-to-watch "has-changed"
delete /node-to-watch
```

## Ceate Zookeeper Quorum 

### Step 01 - create data dictionary for zookeeper

```
# create data dictionary for zookeeper
sudo mkdir -p /data/zookeeper
sudo chown -R ubuntu:ubuntu /data/
```

### Step 02 - declare the server's identity
```
# declare the server's identity
echo "1" > /data/zookeeper/myid
```

### Step 03 - Edit the zookeeper settings
```
# edit the zookeeper settings
nano /home/ubuntu/kafka/config/zookeeper.properties
```

Add following properties to `zookeeper.properties` file.

```
# the location to store the in-memory database snapshots and, unless specified otherwise, the transaction log of updates to the database.
dataDir=/data/zookeeper
# the port at which the clients will connect
clientPort=2181
# disable the per-ip limit on the number of connections since this is a non-production config
maxClientCnxns=0
# the basic time unit in milliseconds used by ZooKeeper. It is used to do heartbeats and the minimum session timeout will be twice the tickTime.
tickTime=2000   # 2 sec
# The number of ticks that the initial synchronization phase can take
initLimit=10
# The number of ticks that can pass between
# sending a request and getting an acknowledgement
syncLimit=5
# zoo servers
# these hostnames such as `zookeeper-1` come from the /etc/hosts file
server.1=zookeeper1:2888:3888
server.2=zookeeper2:2888:3888
server.3=zookeeper3:2888:3888
```

### Step 04 - Restart the zookeeper service
```
sudo service zookeeper stop
sudo service zookeeper start
```

### Step 05 - Check the connectivity 
```
echo "ruok" | nc zookeeper1 2181 ; echo
echo "ruok" | nc zookeeper2 2181 ; echo
echo "ruok" | nc zookeeper3 2181 ; echo
```

### Step 06 - Check the stats 
```
echo "stat" | nc zookeeper1 2181 ; echo
echo "stat" | nc zookeeper2 2181 ; echo
echo "stat" | nc zookeeper3 2181 ; echo
```

### Step 06 - Observe the logs (need to do this on every machine)
```
cat /home/ubuntu/kafka/logs/zookeeper.out | head -100
```


